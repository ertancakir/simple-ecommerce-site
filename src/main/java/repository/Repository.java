package repository;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import java.util.List;

public class Repository {
    protected static SessionFactory factory;

    public Repository(){
        try{
            factory = new Configuration().configure("hibernate.cfg.xml").buildSessionFactory();
        }
        catch (Throwable ex){
            System.out.println("Failed to create session factory " + ex);
        }
    }

    protected void addData(Object object){
        Session session = factory.openSession();
        Transaction transaction = null;
        try{
            transaction = session.beginTransaction();
            session.save(object);
            transaction.commit();
        }
        catch (HibernateException ex){
            System.out.println("We have a problem with adding data :" + ex);
        }
        finally {
            session.close();
        }
    }

    protected List<Object> getAllData(String hql){
        Session session = factory.openSession();
        Transaction transaction = null;
        List<Object> list = null;
        try{
            transaction = session.beginTransaction();
            list = session.createQuery(hql).list();
            transaction.commit();
        }
        catch (HibernateException exception){
            System.out.println("We have a problem with getting data :" + exception);
        }
        finally {
            session.close();
        }
        return list;
    }

    protected Object getDataByValue(String hql){
        Session session = factory.openSession();
        Transaction transaction = null;
        Object data = null;
        try{
            transaction = session.beginTransaction();
            data = session.createQuery(hql).getSingleResult();
        }
        catch (HibernateException exception){
            System.out.println("We have a problem with getting data by value :" + exception);
        }
        finally {
            session.close();
        }
        return data;
    }
}
