package repository;

import domain.User;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;

public class UserRepository extends Repository {

    public void addUser(User user){
        super.addData(user);
    }

    public List<User> getAllUser(){
        String hql = "from User";
        return (List<User>)(Object) super.getAllData(hql);
    }

    public User getUserById(int id){
        String hql = "FROM User U WHERE U.userID = "+ id;
        return (User) super.getDataByValue(hql);
    }
}
