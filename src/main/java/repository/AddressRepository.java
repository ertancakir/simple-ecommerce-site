package repository;

import domain.Address;

public class AddressRepository extends Repository {

    public void addAddressToUser(Address address){
        super.addData(address);
    }
}
