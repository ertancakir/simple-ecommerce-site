package repository;

import domain.Product;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;

public class ProductRepository extends Repository {

    public void addProduct(Product product){
        super.addData(product);
    }
    public List<Product> getAllProduct(){
        String hql = "from Product";
        return (List<Product>)(Object) super.getAllData(hql);
    }
    public Product getProductById(int id){
        String hql = "FROM Product P WHERE P.productID = "+ id;
        return (Product) super.getDataByValue(hql);
    }
}
