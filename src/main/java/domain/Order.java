package domain;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "tbl_order",schema = "public")
public class Order implements Serializable {

    @Column(nullable = false)
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "order_seq")
    @SequenceGenerator(name = "order_seq", sequenceName = "order_sequence", allocationSize = 1)
    private int orderID;

    @ManyToOne(optional = false)
    @JoinColumn(name = "userID",insertable = false , updatable = false)
    private User user;

    @Column(name = "orderAmount")
    private float orderAmount;

    @Column(name = "orderDate")
    private String orderDate;

    @ManyToOne(optional = false, cascade = CascadeType.ALL)
    @JoinColumn(name = "orderID",insertable = false , updatable = false)
    private Address orderAddress;

    @ManyToOne(optional = false, cascade = CascadeType.ALL)
    @JoinColumn(name = "productID")
    private Product product;

    public int getOrderID() {
        return orderID;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public double getOrderAmount() {
        return orderAmount;
    }

    public void setOrderAmount(float orderAmount) {
        this.orderAmount = orderAmount;
    }

    public String getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }



}
