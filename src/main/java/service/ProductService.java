package service;

import domain.Product;
import repository.ProductRepository;

public class ProductService {
    ProductRepository repository;

    public ProductService(){
        repository = new ProductRepository();
    }

    public void addProduct(Product p){
        repository.addProduct(p);
    }
}
