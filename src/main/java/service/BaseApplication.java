package service;

import controller.*;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import java.util.HashSet;
import java.util.Set;

@ApplicationPath("")
public class BaseApplication extends Application {
    private Set<Object> singletons = new HashSet<Object>();

    public BaseApplication() {
        singletons.add(new HomeController());
    }

    @Override
    public Set<Object> getSingletons() {
        return singletons;
    }
}
