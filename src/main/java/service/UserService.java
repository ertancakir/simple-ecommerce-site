package service;

import domain.User;
import repository.UserRepository;

import java.util.List;

public class UserService {
    UserRepository userRepository;
    public UserService(){
        userRepository = new UserRepository();
    }

    public void addUser(User user){
        userRepository.addUser(user);
    }

    public List getAllUsers(){
        List<User> users = userRepository.getAllUser();
        return users;
    }
    public User getUserById(int id){
        User u = userRepository.getUserById(id);
        return u;
    }
}
