package service;

import domain.Address;
import repository.AddressRepository;

public class AddressService {
    AddressRepository repository;
    public AddressService(){
        repository = new AddressRepository();
    }
    public void addAddressToUser(Address address){
        repository.addAddressToUser(address);
    }
}
