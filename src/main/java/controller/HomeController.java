package controller;

import domain.Address;
import domain.Product;
import domain.User;
import service.AddressService;
import service.ProductService;
import service.UserService;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/Home")
public class HomeController {

    @GET
    @Path("/index")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getIndex(){
        UserService userService = new UserService();
        List<User> users = userService.getAllUsers();
        return Response.status(200).entity(users.get(0)).build();
    }

    @POST
    @Path("/addproduct")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.TEXT_HTML)
    public Response addProduct(@FormParam("name") String name,@FormParam("price") float price, @FormParam("stock") int stock){
        ProductService service = new ProductService();

        Product product = new Product();
        product.setProductName(name);
        product.setProductStock(stock);
        product.setProcuctPrice(price);

        service.addProduct(product);
        return Response.status(201).entity("<h1>Ürün Eklendi</h1>").build();
    }

    @POST
    @Path("/adduser")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.TEXT_HTML)
    public Response addUser(@FormParam("name") String name, @FormParam("lastname") String lastname, @FormParam("password") String password, @FormParam("email") String email){
        UserService service = new UserService();

        User user = new User();
        user.setUserEmail(email);
        user.setUserFirstName(name);
        user.setUserLastName(lastname);
        user.setUserPassword(password);

        service.addUser(user);
        return Response.status(201).entity("<h1>Kullanıcı Eklendi</h1>").build();
    }

    @GET
    @Path("/getUser/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getUser(@PathParam("id") int id){
        UserService service = new UserService();
        User user = service.getUserById(id);
        return Response.status(200).entity(user).build();
    }
    @POST
    @Path("/addAddress")
    @Produces(MediaType.TEXT_HTML)
    public Response addAddressToUser(@FormParam("city") String city, @FormParam("district") String district, @FormParam("zipCode") String zipCode, @FormParam("phoneNumber") String phoneNumber, @FormParam("fullAddress") String fullAddress,@FormParam("userID") int userID){
        UserService userService = new UserService();
        User user = userService.getUserById(userID);

        Address address = new Address();
        address.setCity(city);
        address.setDistrict(district);
        address.setFullAddress(fullAddress);
        address.setPhoneNumber(phoneNumber);
        address.setZipCode(zipCode);
        address.setUser(user);

        AddressService addressService = new AddressService();
        addressService.addAddressToUser(address);
        return Response.status(201).entity("<h1>Adres "+ user.getUserFirstName() +" Kullanıcısına Eklendi</h1>").build();
    }

}
